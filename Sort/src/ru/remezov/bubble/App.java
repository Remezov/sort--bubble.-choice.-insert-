package ru.remezov.bubble;

public class App {
    public static void main(String[] args) {
        int count = 10;
        int[] array = new int[count];

        for (int i = 0; i < count; i++) {
            array[i] = (int) (Math.random() * (count+1));
            System.out.print(array[i] + " ");
        }

        int temp;
        for (int i = 0; i < count; i++) {
            for (int j = count - 1; j > i; j--) {
                if (array[j] < array[j - 1]) {
                    temp = array[j];
                    array[j] = array[j - 1];
                    array[j - 1] = temp;
                }
            }
        }

        System.out.println();
        for (int i = 0; i < count; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
