package ru.remezov.choice;

public class App {
    public static void main(String[] args) {
        int count = 10;
        int[] array = new int[count];

        for (int i = 0; i < count; i++) {
            array[i] = (int) (Math.random() * (count+1));
            System.out.print(array[i] + " ");
        }

        int temp;
        for (int i = 0; i < count; i++) {
            for (int j = i + 1; j < count; j++) {
                if (array[i] > array[j]) {
                    temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }

        System.out.println();
        for (int i = 0; i < count; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
